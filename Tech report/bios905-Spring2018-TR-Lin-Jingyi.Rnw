\documentclass{article}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{times}


\textwidth=6.2in
\textheight=8.5in
%\parskip=.3cm
\oddsidemargin=.1in
\evensidemargin=.1in
\headheight=-.3in




\begin{document}
\setkeys{Gin}{width=0.5\textwidth}
\SweaveOpts{concordance=TRUE}

%------------------------------------------------------------
\title{Technical Report}
%------------------------------------------------------------
\author{Jingyi Lin\\Department of Biostatistics and Bioinformatics}
%\date{}

\SweaveOpts{highlight=TRUE, tidy=TRUE, keep.space=TRUE, keep.blank.space=FALSE, keep.comment=TRUE}
\SweaveOpts{prefix.string=Fig}


\maketitle

\begin{abstract}
  This report focus on implementing the boostrap resampling method on the nuclear dataset in \texttt{boot} package. The result of regression coefficient estimation, point prediction and its confidence interval turns out to be very close to the one given by model based approach. Also the compution efficiency can be improved by using \texttt{Rcpp} and \texttt{RcppEigen} for matrix operations. The code for this project can be found on gitlab repository \url{https://gitlab.oit.duke.edu/jl705/bios905-Spring2018-TR-Lin-Jingyi.git}
\end{abstract}

%-------------------------------------------
\section{Introduction}
%--------------------------------------------
\subsection{The Nuclear Dataset}
The nuclear dataset in the boot package contains information on 11 variates of 32 light water reactors. The response variable of interest is the capital cost of construction in millions of dollars(cost), and corresponding covariates are date on which the construction permit was issued(date), the net capacity of the power plant(cap), whether the plant was constructed in the north-east region of the U.S.A(ne), whether use of a cooling tower in the plant(ct), the cumulative number of power plants constructed by each architect-engineer(cum.n) and whether plants has partial turnkey guarantees(pt). The result of fitting a linear model via least square estimation is listed as followed.
$$log(\mbox{cost})\sim \mbox{date}+log(\mbox{cap})+\mbox{ne}+\mbox{ct}+log(\mbox{cum.n})+\mbox{pt}+\epsilon,\epsilon\sim N(0,\sigma^2)$$

<<test1,eval=TRUE,echo=FALSE>>=
library(boot)
nuke <- nuclear[, c(1, 2, 5, 7, 8, 10, 11)]
nuke.lm <- lm(log(cost) ~ date+log(cap)+ne+ct+log(cum.n)+pt, data = nuke)
nuke.lm
@
\subsection{Model Based Prediction}
Given a new obervation where $\mbox{cost} = 1, \mbox{date} = 73.00, \mbox{cap} = 886, \mbox{ne} = 0, \mbox{ct} = 0, \mbox{cum.n} = 11, \mbox{pt} = 1$, the predicted cost biased on our model and its 95\% confidence interval is 6.7194 and (6.1258,7.3130)

<<test2,eval=TRUE,echo=FALSE>>=
new.data <- data.frame(cost = 1, date = 73.00, cap = 886, ne = 0,ct = 0, cum.n = 11, pt = 1)
predict(nuke.lm, new.data,interval= "prediction")
@
\subsection{Model Diagnotics}
And the residuals vs. fitted values plot shows that errors are distributed evenly around zero. The normal probability plot is close to a straight line indicating that the errors are distributed normally.(See appendix for the diagnotic plot.) 

%-------------------------------------------
\section{Bootstrap }
%--------------------------------------------
The bootstrap is a assumption-free method for computing confidence intervals around just about any statistic. When we have doubt on if the normality assumption on the previous linear model is violated, we can use boostrap method to obatin the confidence interval for our point estimated.

We implemented the model fitting, prediction and boostraping process via \texttt{Rcpp} where all matrix operations are executed via the \texttt{RcppEigen} package. The result will be compared to the one given in last section. 
\subsection{Choice of Residual}
The raw residual $e=y-\hat{y}$, which is the difference between the true value and fitted value for the response variable is important statistics for estimating the ramdon error $\epsilon$. While for the purpose of boostaping, a modified version of residual can be more suitable which is 
$$r_i= \frac{e_i}{\sqrt{1-hii}}$$
where $h_ii$ is the diagoal elements of hat matrix. Adopting the modified residual, a normal QQ plot will reveal obvious outlier which can be obsured in a way by the averagin property of raw residual. 
\begin{center}
<<r,eval=TRUE,echo=FALSE,echo=FALSE,fig=TRUE,fig.height=2,>>=
library(rbootstrap)

Y <- log(nuke$cost)
X <- data.frame(1,nuke$date,log(nuke$cap),nuke$ne,nuke$ct,log(nuke$cum.n),nuke$pt)
X <- data.matrix(X)
colnames(X) <- NULL 
x_new <- c(1,73,log(886),0,0,log(11),1)
seed <- 921
R <- 5000
xx = model_predict(Y,X,x_new)
qqnorm(xx$cen_err)
@
\end{center}
\newpage
\subsection{Algorithm}
For $i= 1,2,\cdots, R$, where $R $ is our total resamping times,
\begin{enumerate}
\item Simulate response $y_r^*$ where $Y_r^*$ is the fitted value of response variable plus a random sample of centralized modified residual $r-\bar{r}$
\item Calculated least square estimate $\hat{\beta}_r^*=(X^TX)^{-1}X^Ty_r^*$
\item Ramdomly chose one residual $\epsilon^*$ from the centralized modified residual and calculate predicted error $\delta_i^* = x_+^T(\hat{\beta}_r^*-\hat{\beta}) + \epsilon^*$
\end{enumerate}
See the book \textit{Boostrap Methods and Their Applicaitons, section 6.3.3} for detail explaination. 

\subsection{Result}
\begin{itemize}
  \item The regression coefficient $\hat{\beta}$ estimation is the same 
  


<<r, eval=TRUE,echo=TRUE>>=
xx = model_predict(Y,X,x_new)
beta_hat <- xx$beta_hat
beta_hat
@

  \item The predicted cost $\hat{y}_{new}$ for new observation is the same
<<r, eval=TRUE,echo=TRUE>>=
y_new_hat<- xx$y_new_hat
y_new_hat
@  

  \item The 95\% confidence interval for $\hat{y}_{new}$ is very close.(Resampling is repeated 5000 times.)
<<r, eval=TRUE,echo=FALSE>>=
rboot_cpp <- function(Y,X,x_new,R,seed){
  
  library(rbootstrap)
  n = length(Y)
  xx = model_predict(Y,X,x_new)
  beta_hat <- xx$beta_hat
  Y_hat <- xx$Y_hat
  y_new_hat<- xx$y_new_hat
  cen_err <- xx$cen_err
  
  # Resampling Index
  set.seed(seed)
  err_rsp <- c()
  
  for(i in 1:R){
    cen_err_rsp <- sample(cen_err,size = n, replace = TRUE)
    Y_rsp <- VecSum(Y_hat,cen_err_rsp)
    beta_rsp <- lse(X,Y_rsp)
    err_rsp[i] <- VecInnerProd(x_new,beta_rsp-beta_hat)-sample(cen_err,size = 1)
  }
  
  return(y_new_hat+quantile(err_rsp,c(0.025,0.975)))
  
}
@ 

<<r, eval=TRUE,echo=TRUE>>=
rboot_cpp(Y,X,x_new,R=5000,seed=921)
@  

  \item Comupation efficiency comparison.\\
  \texttt{rboot} and \texttt{rboot\_cpp} are both functions to compute 95\% confidence interval for $\hat{y}_{new}$. \texttt{rboot} is coded by basic \texttt{R} language. \texttt{rboot\_cpp} use \texttt{Rcpp} and \texttt{RcppEigen} for matrix operation. Using \texttt{microbenchmark} to compare the time consumption of this two funciton, we can see that \texttt{rboot\_cpp} are about 50 times faster when the boostrap resampling is repeated $R=5000$ times.

<<r, eval=TRUE,echo=FALSE>>=
library(microbenchmark)
rboot <- function(Y,X,x_new,R,seed){
  
  library(matlib)
  n = length(Y)
  beta_hat <- inv(t(X)%*%X)%*%t(X)%*%Y
  Y_hat <- X%*%beta_hat
  mat_hat <- X%*%inv(t(X)%*%X)%*%t(X)
  h <- diag(mat_hat)
  raw_err <- Y - Y_hat
  mod_err <- raw_err/sqrt(1-h)
  cen_err <- mod_err - mean(mod_err)
  y_new_hat <- x_new%*%beta_hat

  
  # Resampling Index
  set.seed(seed)
  err_rsp <- c()
  
  for(i in 1:R){
    cen_err_rsp <- sample(cen_err,size = n, replace = TRUE)
    Y_rsp <- Y_hat + cen_err_rsp
    beta_rsp <- inv(t(X)%*%X)%*%t(X)%*%Y_rsp
    err_rsp[i] <- x_new%*%(beta_rsp-beta_hat)-sample(cen_err,size = 1)
  }
  
  return(rep(y_new_hat,2)+quantile(err_rsp,c(0.025,0.975)))
  
}
@

<<r, eval=TRUE,echo=TRUE>>=
microbenchmark(a = rboot(Y,X,x_new,R,seed),b=rboot_cpp(Y,X,x_new,R,seed),times = 100)
@   
  
 \end{itemize}


\newpage
%------------------------------------
%handy to include this at the end



%-------------------------------------------
\section{Appendix}
%--------------------------------------------
%------------------------------------
\subsection{SessionInfo}
%-------------------------------------

<<sessionInfo,eval=TRUE,echo=FALSE,results=verbatim>>=
sessionInfo();
@ 

\subsection{Fitted and residuals - raw residual}
<<test1,eval=TRUE,echo=FALSE,fig=TRUE,fig.height=2>>=
plot(nuke.lm,which=1,main=NULL,ann = FALSE,sub.caption="")
@
\subsection{QQ Plot - raw residual}
<<test2,eval=TRUE,echo=FALSE,fig=TRUE,fig.height=2>>=
plot(nuke.lm,which=2,main=NULL,ann = FALSE,sub.caption="")
@


\end{document}